package cn.tedu.charging.user.dao.repository;

import cn.tedu.charging.user.pojo.po.UserPO;


public interface UserRepository {
    UserPO getUserInfoByOpenId(String openId);

    Integer save(UserPO userPO);
}
