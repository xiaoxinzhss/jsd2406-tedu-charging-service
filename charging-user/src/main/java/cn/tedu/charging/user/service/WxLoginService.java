package cn.tedu.charging.user.service;

public interface WxLoginService {
    Integer login(String code, String nickName);
}
