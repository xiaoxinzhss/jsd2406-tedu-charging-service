package cn.tedu.charging.user.service;

import cn.tedu.charing.common.pojo.vo.UserInfoVO;

public interface UserService {
    UserInfoVO getUserInfo(Integer userId);
}
