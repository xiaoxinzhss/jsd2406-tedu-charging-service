package cn.tedu.charging.user.dao.repository.impl;

import cn.tedu.charging.user.dao.mapper.UserMapper;
import cn.tedu.charging.user.dao.repository.UserRepository;
import cn.tedu.charging.user.pojo.po.UserPO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Slf4j
@Repository
public class UserRepositoryImpl implements UserRepository {

    @Autowired
    private UserMapper userMapper;

    @Override
    public UserPO getUserInfoByOpenId(String openId) {
        QueryWrapper queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("wx_open_id",openId);
        return userMapper.selectOne(queryWrapper);
    }

    @Override
    public Integer save(UserPO userPO) {
        log.debug("plus 保存 userPO之前:{}",userPO);
        //row 表示影响行数,update,delete,insert
        Integer row = userMapper.insert(userPO);
        log.debug("plus 保存 userPO之后,会自动把id填充到PO:{}",userPO);
        //mybaits下需要通过 如果向返回自增id,mapper XML insert 标签
        // useGeneratedKeys keyProperty 属性
        //<insert id="insertData" useGeneratedKeys="true" keyProperty="id">
        //  INSERT INTO your_table (column1, column2, ...)
        //  VALUES (#{value1}, #{value2}, ...)
        //</insert>
        return userPO.getId();
    }
}
