package cn.tedu.charging.user.controller;

import cn.tedu.charging.user.service.WxLoginService;
import cn.tedu.charing.common.pojo.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class WechatLoginController {

    @Autowired
    private WxLoginService wxLoginService;

    @GetMapping("/wx/login")
    public JsonResult<Integer> wxLogin(@RequestParam("code") String code,
                                       @RequestParam("nickName") String nickName){
        Integer userId = wxLoginService.login(code,nickName);
        return JsonResult.ok(userId);

    }
}
