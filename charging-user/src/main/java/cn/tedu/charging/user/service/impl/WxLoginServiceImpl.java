package cn.tedu.charging.user.service.impl;

import cn.tedu.charging.user.dao.repository.UserRepository;
import cn.tedu.charging.user.pojo.WxLoginData;
import cn.tedu.charging.user.pojo.po.UserPO;
import cn.tedu.charging.user.service.WxLoginService;
import cn.tedu.charing.common.utils.JsonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class WxLoginServiceImpl implements WxLoginService {

    private static final Logger log = LoggerFactory.getLogger(WxLoginServiceImpl.class);
    /**
     * appId 替换成你的appId ,应该放到配置文件里
     */
    private String appId = "wxf7546f9b87546078";

    /**
     * 小程序密钥 替换成你的AppSecret  应该放到配置文件里
     */
    private String appSecret = "439f1504f2bf53ae5d3dcbb5657bc7e1";

    @Autowired
    private UserRepository userRepository;

    /**
     * OpenId 微信用户的唯一标识,你只要有微信,登录成功过,就有一个openId
     *
     * AppId 微信小程序的唯一标识,就有一个AppId
     *
     * 通过微信的唯一标识 openId 来 充电服务的用户系统的用户表 判断 是否存在
     * 不存在 注册 保存 昵称 和 openId 返回表 数据库表自增的id,生成一个唯一的充电服务用户的唯一标识
     * 存在  返回微信唯一标识的 openId 对应的 充电服务用户的唯一标识
     * @param code
     * @param nickName
     * @return
     */
    @Override
    public Integer login(String code, String nickName) {
        //调用微信的接口,获取用户的openId
        String openId = getOpenId(code);
        if (openId != null) {
            //通过openId获取用户信息
            UserPO userPO = userRepository.getUserInfoByOpenId(openId);
            if (userPO == null) {
                //注册功能
                //为空,表示用户第一次登录我们的系统, 注册 用户信息
                //把微信用户的信息openId,昵称 保存到用户表,返回用户id
                userPO = new UserPO();
                userPO.setWxOpenId(openId);
                userPO.setNickName(nickName);
                return userRepository.save(userPO);
            }else {
                //不为空,已经注册过,表示用户不是第一次登录我们的系统,直接返回用户id
                return userPO.getId();
            }
        }
        return -1;//-1表示登录失败
    }

    @Autowired
    private RestTemplate restTemplate;

    /**
     * api.weixin.qq.com/sns/jscode2session 可以通过微信的一个接口 传入 code 获取 openId
     * 一个接口需要鉴权 ,不是任何人都能调用的
     * 必须称为 微信小程序开发者,申请appId ,有了appId appSecret 可以调用接口 来 通过code 换 openId
     * @param code
     * @return
     */
    private String getOpenId(String code) {
        //通过微信的接口地址和appidAppID(小程序ID)和 appSecret AppSecret(小程序密钥)
        String url = "https://api.weixin.qq.com/sns/jscode2session?" +
                "appid=" + appId +
                "&secret=" + appSecret +
                "&js_code=" + code +
                "&grant_type=authorization_code";
        String jsonData =
                restTemplate.getForObject(url, String.class);
        log.debug("调用微信地址获取用户信息:{}",jsonData);
        WxLoginData wxLoginData = JsonUtils.fromJson(jsonData, WxLoginData.class);
        if (wxLoginData != null) {
            return wxLoginData.getOpenid();
        }
        return null;
    }
}
