package cn.tedu.charging.user.controller;

import cn.tedu.charing.common.pojo.vo.UserInfoVO;
import cn.tedu.charging.user.service.UserService;
import cn.tedu.charing.common.pojo.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("user")
@Api(tags = "用户接口")
public class UserController {

    @Autowired
    private UserService userService;

    @ApiOperation("获取用户信息")
    @GetMapping("/info/{userId}")
    public JsonResult<UserInfoVO> getUserInfo(@PathVariable("userId") Integer userId) {
        log.debug("获取用户信息:{}",userId);
        UserInfoVO userInfoVO = userService.getUserInfo(userId);
        JsonResult ok = JsonResult.ok(userInfoVO);
        log.debug("获取用户信息:{},结果:{}",userId,ok);
        return ok;
    }

}
