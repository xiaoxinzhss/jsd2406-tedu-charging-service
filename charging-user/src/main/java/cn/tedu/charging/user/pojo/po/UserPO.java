package cn.tedu.charging.user.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@TableName("charging_user_info")
public class UserPO {

    @TableId(type = IdType.AUTO)
    Integer id;

    /**
     * 昵称
     */
    String nickName;

    /**
     * 微信openId
     */
    String wxOpenId;
}
