package cn.tedu.charging.device.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@TableName("charging_station")
public class StationPO {

    @TableId(type = IdType.AUTO)
    Integer id;

    /**
     * 基本信息 场站名称
     */
    String stationName;

    /**
     *  位置信息 经度
     */
    BigDecimal stationLng;

    /**
     * 位置信息 维度
     */
    BigDecimal stationLat;


}
