package cn.tedu.charging.device.controller;

import cn.tedu.charing.common.pojo.param.GunStatusUpdateParam;
import cn.tedu.charging.device.service.DeviceService;
import cn.tedu.charing.common.pojo.vo.StationInfoVO;
import cn.tedu.charing.common.pojo.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("device")
@Api(tags = "设备接口")
public class DeviceController {

    @Autowired
    private DeviceService deviceService;

    @ApiOperation("获取场站信息")
    @GetMapping("/station/info/{gunId}")
    public JsonResult<StationInfoVO> getUserInfo(@PathVariable("gunId") Integer gunId) {
        log.debug("获取场站信息:{}",gunId);
        StationInfoVO userInfoVO = deviceService.getStationInfo(gunId);
        JsonResult ok = JsonResult.ok(userInfoVO);
        log.debug("获取场站信息:{},结果:{}",gunId,ok);
        return ok;
    }


    @ApiOperation("更新枪状态")
    @PostMapping("/station/gun/status/update")
    public JsonResult<Boolean> updateGunStatus(@RequestBody GunStatusUpdateParam param) {
        log.debug("更新枪状态:{}",param);
        Boolean success = deviceService.updateGunStatus(param);
        JsonResult ok = JsonResult.ok(success);
        log.debug("更新枪状态:{},结果:{}",param,ok);
        return ok;
    }


    @ApiOperation("附近充电站")
    @GetMapping("/station/near")
    public JsonResult<List<StationInfoVO>> getNearStation(Double longitude,
                                                          Double latitude,
                                                          Double distance) {
        log.debug("获取附近充电站:经度{},维度:{},距离:{}",
                longitude,latitude,distance);
        List<StationInfoVO> userInfoVOs =
                deviceService.getNearStation(longitude,latitude,distance);
        JsonResult ok = JsonResult.ok(userInfoVOs);
        log.debug("获取附近充电站:经度{},维度:{},距离:{},结果:{}",
                longitude,latitude,distance,ok);
        return ok;
    }



}
