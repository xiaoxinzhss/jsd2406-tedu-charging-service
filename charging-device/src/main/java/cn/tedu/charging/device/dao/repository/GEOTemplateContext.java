package cn.tedu.charging.device.dao.repository;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class GEOTemplateContext {

    @Value("${station.warm.dataType}")
    private String dataType;

    @Autowired
    @Qualifier("GEOANDGEO")
    private StationCacheTemplateRepository geoAndGeoCacheTemplate;

    @Autowired
    @Qualifier("GEOANDNOSHAREHASH")
    private StationCacheTemplateRepository geoAndNoShareCacheTemplate;

    @Autowired
    @Qualifier("GEOANDSHAREHASH")
    private StationCacheTemplateRepository geoAndShareHashCacheTemplate;

    @Autowired
    @Qualifier("GEOANDSTRING")
    private StationCacheTemplateRepository getAndStringCacheTemplate;




    public StationCacheTemplateRepository geoRepositoryTemplate(){
        log.debug("GEO使用的数据类型:{}",dataType);
        if ("GEOANDGEO".equals(dataType)) {
            return geoAndGeoCacheTemplate;
        }else if ("GEOANDSTRING".equals(dataType)) {
            return getAndStringCacheTemplate;
        }else if ("GEOANDSHAREHASH".equals(dataType)) {
            return geoAndShareHashCacheTemplate;
        }else if ("GEOANDNOSHAREHASH".equals(dataType)) {
            return geoAndNoShareCacheTemplate;
        }
        return null;
    }
}
