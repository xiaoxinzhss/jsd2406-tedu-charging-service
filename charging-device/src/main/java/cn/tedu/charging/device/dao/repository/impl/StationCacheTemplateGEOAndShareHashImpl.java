package cn.tedu.charging.device.dao.repository.impl;

import cn.tedu.charging.device.dao.repository.StationCacheTemplateRepository;
import cn.tedu.charging.device.pojo.StationInfoWrapper;
import cn.tedu.charging.device.pojo.po.StationPO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import java.util.*;

/**
 * 位置信息和基本信息建议分开存  基本信息可以复用
 *
 * 基本信息 可以通过什么类型存 ? 更加合理
 * String
 * Hash独享 一个站 一个hash
 * Hash共享 所有站 一个hash
 *
 * 一个类 可以  实现多个接口
 * 一个类 只能继承 一个类
 *
 * 一个类同时 可以 实现接口 并且 继承一个类
 * 这个实现这个接口同时 取复用 父类里的方法
 *
 *
 */
@Slf4j
@Repository("GEOANDSHAREHASH")
public class StationCacheTemplateGEOAndShareHashImpl
        extends AbstractStationCacheTemplate implements StationCacheTemplateRepository {

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public void save(List<StationPO> stationPOS) {
        saveGEOByRedisGEOAndMemberIsId(stationPOS);
        saveStationInfoByHashShare(stationPOS);
    }

    public void saveStationInfoByHashShare(List<StationPO> stationPOs){
        HashOperations hashOperations = redisTemplate.opsForHash();
        String key = "station_infos_hash_share";
        Map<String,StationPO> stationPOMap = new HashMap<>();
        for (StationPO stationPO : stationPOs) {
            stationPOMap.put(String.valueOf(stationPO.getId()),stationPO);
        }
        hashOperations.putAll(key,stationPOMap);
    }

    @Override
    public List<StationInfoWrapper> search(Double longitude, Double latitude, Double distance) {
        return super.search(longitude,latitude,distance);
    }

    @Override
    public void insert(StationPO stationPO) {

    }

    @Override
    public void update(StationPO stationPO) {

    }

    @Override
    public void delete(StationPO stationPO) {

    }

    @Override
    public StationPO getStationInfoById(String stationId) {
        HashOperations<String,Integer,StationPO> hashOperations =
                redisTemplate.opsForHash();
        String key = "station_infos_hash_share";
        //购物车 10商品数量,hashKey 商品id key 用户id
        //hashOperations.increment(key,hashKey,10)
        //hash 是可以通过大key和hashkey 获取 hashvalue
        return hashOperations.get(key, stationId);
    }

    @Override
    public RedisTemplate getRedisTemplate() {
        return redisTemplate;
    }
}
