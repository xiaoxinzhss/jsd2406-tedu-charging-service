package cn.tedu.charging.device.canal;

import cn.tedu.charging.device.pojo.po.StationCanalPO;
import cn.tedu.charging.device.service.DeviceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import top.javatool.canal.client.annotation.CanalTable;
import top.javatool.canal.client.handler.EntryHandler;

/**
 * 场站数据的同步处理器
 * 数据库中的表 @CanalTable    charging_station  要和Redis保存一致
 * charging_station 发生了变化  insert update delete
 *
 * charging_station 插入数据 通知 StationSyncHandler 调用  insert Redis里插入数据
 * charging_station 更新数据 通知 StationSyncHandler 调用  update Redis里更新数据
 * charging_station 删除数据 通知 StationSyncHandler 调用  insert Redis里删除数据
 *
 */
@Slf4j
@Component
@CanalTable("charging_station")
public class StationSyncHandler implements EntryHandler<StationCanalPO> {

    @Autowired
    private DeviceService deviceService;

    @Override
    public void insert(StationCanalPO stationCanalPO) {
        log.debug("数据表 charging_station 插入了新的数据:{}",stationCanalPO);
        //通过 redisTemplate 插入新的数据, 缓存预热用的是什么数据,在对应的数据里执行插入动作
        deviceService.insert(stationCanalPO);
        EntryHandler.super.insert(stationCanalPO);
    }

    @Override
    public void update(StationCanalPO before, StationCanalPO after) {
        log.debug("数据表 charging_station 更新了数据,更新之前:{},更新之后:{}",before,after);
        //通过 redisTemplate 插入新的数据, 缓存预热用的是什么数据,在对应的数据里执行更新动作
        deviceService.update(after);
        EntryHandler.super.update(before, after);
    }

    @Override
    public void delete(StationCanalPO stationCanalPO) {
        log.debug("数据表 charging_station 删除了数据:{}",stationCanalPO);
        //通过 redisTemplate 插入新的数据, 缓存预热用的是什么数据,在对应的数据里执行删除动作
        deviceService.delete(stationCanalPO);
        EntryHandler.super.delete(stationCanalPO);
    }
}
