package cn.tedu.charging.device.dao.repository.impl;

import cn.tedu.charging.device.dao.mapper.StationMapper;
import cn.tedu.charging.device.dao.repository.StationRepository;
import cn.tedu.charging.device.pojo.po.StationPO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;

@Repository("d")
public class StationRepositoryImpl implements StationRepository {

    @Autowired
    private StationMapper stationMapper;

    @Override
    public List<StationPO> getAllStation() {
        // selectList queryWrapper 用来指定 查询条件
        //如果传null 表示没有查询条件 查询所有
        return stationMapper.selectList(null);
    }
}
