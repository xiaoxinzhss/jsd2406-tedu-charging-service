package cn.tedu.charging.device.dao.repository.impl;

import cn.tedu.charging.device.dao.repository.StationCacheTemplateRepository;
import cn.tedu.charging.device.pojo.StationInfoWrapper;
import cn.tedu.charging.device.pojo.po.StationPO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundGeoOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Repository;

import java.util.*;

/**
 * 位置信息和基本信息建议分开存  基本信息可以复用
 *
 * 基本信息 可以通过什么类型存 ? 更加合理
 * String
 * Hash独享 一个站 一个hash
 * Hash共享 所有站 一个hash
 *
 *
 *
 */
@Slf4j
@Repository("GEOANDSTRING")
public class StationCacheTemplateGEOAndStringImpl
        extends AbstractStationCacheTemplate
        implements StationCacheTemplateRepository {

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public void save(List<StationPO> stationPOS) {
        saveGEOByRedisGEOAndMemberIsId(stationPOS);
        saveStationInfoByString(stationPOS);
    }

    private void saveStationInfoByString(List<StationPO> stationPOS) {
        Map<String,StationPO> stationPOMap = new HashMap<>();
        for (StationPO stationPO : stationPOS) {
            String key = "station_info_str_" + stationPO.getId();
            stationPOMap.put(key,stationPO);
        }
        ValueOperations valueOperations = redisTemplate.opsForValue();
        valueOperations.multiSet(stationPOMap);
    }

    @Override
    public List<StationInfoWrapper> search(Double longitude, Double latitude, Double distance) {
        return super.search(longitude,latitude,distance);
    }

    @Override
    public void insert(StationPO stationPO) {
        //List<StationPO> stationPOS = new ArrayList<>();
        //stationPOS.add(stationPO);
        //List<StationPO> list = Arrays.asList(stationPO);
        save(Arrays.asList(stationPO));
    }

    @Override
    public void update(StationPO stationPO) {
        ValueOperations valueOperations = redisTemplate.opsForValue();
        String key = "station_info_str_" + stationPO.getId();
        //key 存在 更新, 不存在 插入
        valueOperations.set(key,stationPO);
    }

    @Override
    public void delete(StationPO stationPO) {
        String key = "station_info_str_" + stationPO.getId();
        redisTemplate.delete(key);
        //删除基本信息,还有删除位置信息
        BoundGeoOperations stations = getRedisTemplate().boundGeoOps("stations");
        stations.remove(stationPO.getId());
    }

    @Override
    public StationPO getStationInfoById(String stationId) {
        String key = "station_info_str_" + stationId;
        ValueOperations<String,StationPO> valueOperations
                = redisTemplate.opsForValue();
        return valueOperations.get(key);
    }

    @Override
    public RedisTemplate getRedisTemplate() {
        return redisTemplate;
    }
}
