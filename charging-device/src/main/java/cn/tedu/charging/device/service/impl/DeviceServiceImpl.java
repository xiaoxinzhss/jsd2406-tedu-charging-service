package cn.tedu.charging.device.service.impl;

import cn.tedu.charging.device.dao.repository.StationCacheTemplateRepository;
import cn.tedu.charging.device.pojo.StationInfoWrapper;
import cn.tedu.charging.device.pojo.po.StationCanalPO;
import cn.tedu.charging.device.pojo.po.StationPO;
import cn.tedu.charging.device.dao.repository.GEOTemplateContext;
import cn.tedu.charing.common.pojo.param.GunStatusUpdateParam;
import cn.tedu.charging.device.service.DeviceService;
import cn.tedu.charing.common.pojo.vo.StationInfoVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class DeviceServiceImpl implements DeviceService {

    @Override
    public StationInfoVO getStationInfo(Integer gunId) {
        StationInfoVO stationInfoVO = new StationInfoVO();
        stationInfoVO.setStationId(99);
        return stationInfoVO;
    }

    @Override
    public Boolean updateGunStatus(GunStatusUpdateParam param) {
        return true;
    }


    @Autowired
    private GEOTemplateContext geoTemplateContext;



    @Override
    public List<StationInfoVO> getNearStation(Double longitude, Double latitude, Double distance) {
        StationCacheTemplateRepository repository = getCacheTemplateRepository();
        List<StationInfoWrapper> nearStation = repository.search(longitude, latitude, distance);
        List<StationInfoVO> stationInfoVOList = new ArrayList<>();
        for (StationInfoWrapper stationInfo : nearStation) {
            StationInfoVO stationInfoVO = new StationInfoVO();
            stationInfoVO.setStationId(stationInfo.getStationPO().getId());
            stationInfoVO.setStationName(stationInfo.getStationPO().getStationName());
            stationInfoVO.setStationLat(stationInfo.getStationPO().getStationLat());
            stationInfoVO.setStationLng(stationInfo.getStationPO().getStationLng());
            //设置动态的距离
            stationInfoVO.setDistance(stationInfo.getDistance());
            stationInfoVOList.add(stationInfoVO);
        }
        return stationInfoVOList;
    }

    private StationCacheTemplateRepository getCacheTemplateRepository() {
        StationCacheTemplateRepository repository =
                geoTemplateContext.geoRepositoryTemplate();
        log.debug("获取场站处理模板:{}", repository);
        return repository;
    }

    @Override
    public void save(List<StationPO> stationPOS) {
        StationCacheTemplateRepository repository = getCacheTemplateRepository();
        repository.save(stationPOS);
    }

    @Override
    public void insert(StationCanalPO stationCanalPO) {
        StationCacheTemplateRepository repository = getCacheTemplateRepository();
        StationPO stationPO = new StationPO();
        BeanUtils.copyProperties(stationCanalPO,stationPO);
        repository.insert(stationPO);
    }

    @Override
    public void update(StationCanalPO stationCanalPO) {
        StationCacheTemplateRepository repository = getCacheTemplateRepository();
        StationPO stationPO = new StationPO();
        BeanUtils.copyProperties(stationCanalPO,stationPO);
        repository.update(stationPO);
    }

    @Override
    public void delete(StationCanalPO stationCanalPO) {
        StationCacheTemplateRepository repository = getCacheTemplateRepository();
        StationPO stationPO = new StationPO();
        BeanUtils.copyProperties(stationCanalPO,stationPO);
        repository.delete(stationPO);
    }
}
