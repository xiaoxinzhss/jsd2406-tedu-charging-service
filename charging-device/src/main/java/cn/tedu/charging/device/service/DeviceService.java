package cn.tedu.charging.device.service;

import cn.tedu.charging.device.pojo.po.StationCanalPO;
import cn.tedu.charging.device.pojo.po.StationPO;
import cn.tedu.charing.common.pojo.param.GunStatusUpdateParam;
import cn.tedu.charing.common.pojo.vo.StationInfoVO;

import java.util.List;

public interface DeviceService {
    StationInfoVO getStationInfo(Integer gunId);

    Boolean updateGunStatus(GunStatusUpdateParam param);

    List<StationInfoVO> getNearStation(Double longitude, Double latitude, Double distance);

    void save(List<StationPO> stationPOS);


    void update(StationCanalPO stationCanalPO);

    void delete(StationCanalPO stationCanalPO);

    void insert(StationCanalPO stationCanalPO);
}
