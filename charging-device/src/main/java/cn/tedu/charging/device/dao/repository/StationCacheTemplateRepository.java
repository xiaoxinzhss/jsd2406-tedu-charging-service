package cn.tedu.charging.device.dao.repository;

import cn.tedu.charging.device.pojo.StationInfoWrapper;
import cn.tedu.charging.device.pojo.po.StationPO;

import java.util.List;

public interface StationCacheTemplateRepository {
    /**
     * 保存数据 保存场站信息 到 Redis
     */
    void save(List<StationPO> stationPOS);

    /**
     * 查询数据
     */
    List<StationInfoWrapper> search(Double longitude, Double latitude, Double distance);

    void insert(StationPO stationPO);

    void update(StationPO stationPO);

    void delete(StationPO stationPO);
}
