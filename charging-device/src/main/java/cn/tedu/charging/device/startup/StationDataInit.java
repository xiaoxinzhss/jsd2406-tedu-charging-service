package cn.tedu.charging.device.startup;


import cn.tedu.charging.device.dao.repository.StationRepository;
import cn.tedu.charging.device.pojo.po.StationPO;
import cn.tedu.charging.device.service.DeviceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * 缓存预热
 * 在项目启动的时候 计价服务启动的时候
 *      ApplicationRunner spring容器启动完成之后，就会紧接着执行这个接口实现类的run方法。
 *      @PostConstruct 构建bean的时候
 * 从数据库 查询场站信息 charging_station
 * 基本信息 (名称,地址...)
 * 位置信息 (经纬度)
 * 存储到Redis
 * 基本信息 怎么存? Redis基本数据类型
 * 位置信息 GEO
 */
@Slf4j
@Component
public class StationDataInit implements ApplicationRunner {

 /*   @PostConstruct
    public void init() {
        log.info("PostConstruct init");
    }*/

    @Autowired
    private StationRepository stationRepository;

    @Autowired
    private DeviceService  deviceService;


    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("ApplicationRunner#run init ");
        log.debug("缓存预热 把数据库的数据缓存到Redis," +
                "在设备服务启动的时候,从数据库查询场站信息 位置信息(经纬度),基本信息(名称...),存入到Redis");

        log.debug("从数据库查询场站信息");
        //全部查询 如果数据量过大,内存溢出 OutMemoryException
        //https://blog.csdn.net/TreeShu321/article/details/135255821
        //面试题 说说你工作中遇到了什么印象深刻的事情? 内存溢出
        //分页查询
        // limit 两个参数 limit 1 (从哪开始),5 (查几个)
        // limit 一个参数 limit 1  0(忽略),5 (查几个)
        List<StationPO> stationPOS = stationRepository.getAllStation();
        log.debug("从数据库查询场站信息,结果:{}", stationPOS);
        if (!CollectionUtils.isEmpty(stationPOS)) {
            deviceService.save(stationPOS);
        }else {
            log.debug("为找到场站信息,告警告警告警");
        }
    }
}
