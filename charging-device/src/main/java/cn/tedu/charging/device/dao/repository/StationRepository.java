package cn.tedu.charging.device.dao.repository;

import cn.tedu.charging.device.pojo.po.StationPO;

import java.util.List;

public interface StationRepository {
    List<StationPO> getAllStation();
}
