package cn.tedu.charging.device.dao.mapper;

import cn.tedu.charging.device.pojo.po.StationPO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface StationMapper extends BaseMapper<StationPO> {
}
