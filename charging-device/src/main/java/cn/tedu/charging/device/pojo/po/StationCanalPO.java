package cn.tedu.charging.device.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.persistence.Column;
import java.math.BigDecimal;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class StationCanalPO {

    Integer id;

    /**
     * 基本信息 场站名称
     */
    @Column(name = "station_name")
    String stationName;

    /**
     *  位置信息 经度
     */
    @Column(name = "station_lng")
    BigDecimal stationLng;

    /**
     * 位置信息 维度
     */
    @Column(name = "station_lat")
    BigDecimal stationLat;


}
