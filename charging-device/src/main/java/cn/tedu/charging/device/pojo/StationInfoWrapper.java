package cn.tedu.charging.device.pojo;

import cn.tedu.charging.device.pojo.po.StationPO;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

/**
 * 封装了PO 和 真实的距离
 */
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class StationInfoWrapper {

    StationPO stationPO;

    Double distance;
}
