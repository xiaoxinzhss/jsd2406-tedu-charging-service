package cn.tedu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChargingGatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(ChargingGatewayApplication.class,args);
    }
}