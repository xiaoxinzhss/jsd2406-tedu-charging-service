package cn.tedu.charging.order;

import cn.tedu.charging.order.rabbit.RabbitMQOrderProducer;
import com.rabbitmq.client.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

@SpringBootTest
public class RabbitMQTests {

    @Autowired
    private RabbitMQOrderProducer rabbitMQOrderProducer;

    @Test
    public void publishByRabbitTemplate() {
        //rabbitMQOrderProducer.sendOrder("hello-rabbit-template");
    }

    @Test
    public void publishDirect() throws IOException, TimeoutException {
        Connection connection = getConnection();
        //通过连接创建Channel
        Channel channel = connection.createChannel();
        //通过观察 Channel 可以操作 Exchange Queue
        //topic 在RabbitMQ 里 是怎么个存在?
        String exchange = "hello-exchange";
        String routingKey = "routingKey";
        String queue = "hello-queue";
        AMQP.BasicProperties properties = null;
        String message = "hello-rabbit";
        try {
            //定义 exchange
            channel.exchangeDeclare(exchange, BuiltinExchangeType.DIRECT);
            //定义了 queue
            channel.queueDeclare(queue, true, false, false, null);
            //通过routingKey把Exchange和queue进行绑定
            channel.queueBind(queue, exchange, routingKey);

            channel.basicPublish(exchange,routingKey,properties,message.getBytes());

            //通过channel发送消息
            //1 发送到指定的exchange ,routingKey 发送成功
            //channel.basicPublish(directExchangeName,routingKey,null,message.getBytes());
            //2 发送到指定的exchange ,不指定 routingKey 不会报错,消息不会转发到queue,消息丢失
            //channel.basicPublish(directExchangeName,"",null,message.getBytes());
            //3 发送到指定的exchange, 指定不存在routingKey 不会报错,消息不会转发到queue,消息丢失
            //channel.basicPublish(directExchangeName,"xxxaaabbccc",null,message.getBytes());
            //总结 Direct
            // 1 exchange 必须指定,如果指定不存在的 exchange 报错抛异常 404
            // 2 必须指定 routingKey 如果不指定,或者指定一个不存在的routingKey 消息丢失 不报错
            // 3 不指定 exchange 只指定 routingKey routingKey 会被视为queue,queue存在发送成功,不存在不报错,消息丢失
            // 4 必须指定正确的 exchange routingKey 消息才能发送成功到 exchange,exchange转发到通过routingKey绑定的queue

        }catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("发送成功");

    }

    @Test
    public void publishFanout() throws IOException, TimeoutException {
        Connection connection = getConnection();
        //通过连接创建Channel
        Channel channel = connection.createChannel();
        //通过观察 Channel 可以操作 Exchange Queue
        //topic 在RabbitMQ 里 是怎么个存在?
        String exchange = "hello-exchange";
        String routingKey = "routingKey";
        String queue = "hello-queue";
        AMQP.BasicProperties properties = null;
        String message = "hello-rabbit";
        try {
            //定义 exchange
            channel.exchangeDeclare(exchange, BuiltinExchangeType.FANOUT);
            //定义了 queue
            channel.queueDeclare(queue, true, false, false, null);
            //通过routingKey把Exchange和queue进行绑定
            channel.queueBind(queue, exchange, routingKey);

            channel.basicPublish(exchange,routingKey,properties,message.getBytes());

            //发送消息
            //1 指定fanout类型的exchange，不指定routingKey，结果 和exchange绑定的queue,全部收到消息
            //channel.basicPublish(fanoutExchange,"",null,message.getBytes());
            //2 指定fanout类型的exchange，指定不属于当前Exchange的routingKey，结果和exchange绑定的queue,全部收到消息
            //channel.basicPublish(fanoutExchange,"direct_routing_key",null,message.getBytes());
            //3 指定fanout类型的exchange，指定属于当前Exchange的routingKey，结果 和exchange绑定的queue,全部收到消息
            //channel.basicPublish(fanoutExchange,"hello_fanout_routing_key_0",null,message.getBytes());
            //4 指定fanout类型的exchange，指定不存在routingKey，结果 和exchange绑定的queue,全部收到消息
            //String notExistRoutingKey = "aadfsadfsdf";
            //channel.basicPublish(fanoutExchange,notExistRoutingKey,null,message.getBytes());

            //总结 必须指定存在的 fanout类型 exchange, routingKey 不传,传对,传错, 全发 ,忽略 routingKey

            // 5 发送给不存在的 exchange  报异常 (reply-code=404, reply-text=NOT_FOUND - no exchange 'xxx' in vhost '/'
            //try {
            //    channel.basicPublish("xxx","xxx",null,message.getBytes());
            //}catch (Exception e) {
            //    e.printStackTrace();
            //}
            //6 发送 不指定 exchange 不指定  routingKey  消息丢失
        }catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("发送成功");
    }


    /**
     * 发消息
     */
    @Test
    public void publish() throws IOException, TimeoutException {
        Connection connection = getConnection();
        //通过连接创建Channel
        Channel channel = connection.createChannel();
        //通过观察 Channel 可以操作 Exchange Queue

        //topic 在RabbitMQ 里 是怎么个存在?
        String exchange = "hello-exchange";
        String routingKey = "routingKey";
        String queue = "hello-queue";
        AMQP.BasicProperties properties = null;
        String message = "hello-rabbit";
        try {
            //定义 exchange
            channel.exchangeDeclare(exchange, BuiltinExchangeType.DIRECT);
            //定义了 queue
            channel.queueDeclare(queue, true, false, false, null);
            //通过routingKey把Exchange和queue进行绑定
            channel.queueBind(queue, exchange, routingKey);
            channel.basicPublish(exchange,routingKey,properties,message.getBytes());
        }catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("发送成功");
    }

    private static Connection getConnection() throws IOException, TimeoutException {
        //创建连接的工厂 ,工厂模式 设计模式 抽象工厂,简单工厂...
        //工厂是用来生产东西的, 连接工厂是用来生产连接的
        ConnectionFactory factory = new ConnectionFactory();
        factory.setVirtualHost("/");
        factory.setHost("localhost");
        factory.setPort(5672); //15672 管理的端口
        factory.setUsername("guest");
        factory.setPassword("guest");
        //通过工厂创建连接
        Connection connection = factory.newConnection();
        return connection;
    }

    /**
     * 收消息
     */
    @Test
    public void subscribe() throws IOException, TimeoutException {
        //获取连接
        Connection connection = getConnection();
        //通过连接创建channel
        Channel channel = connection.createChannel();

        String queue = "hello-queue";

        DeliverCallback deliverCallback = (consumerTag, message) -> {
            byte[] body = message.getBody();
            String msg = new String(body);
            System.out.println(msg);
        };
        channel.basicConsume(queue,true,deliverCallback,consumerTag -> {});
    }
}
