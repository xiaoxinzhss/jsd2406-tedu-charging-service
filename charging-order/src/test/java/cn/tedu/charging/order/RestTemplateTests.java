package cn.tedu.charging.order;

import cn.tedu.charging.order.pojo.param.OrderAddParam;
import cn.tedu.charing.common.pojo.JsonResult;
import cn.tedu.charing.common.pojo.vo.UserInfoVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Random;

@SpringBootTest
public class RestTemplateTests {

    @Autowired
    private RestTemplate restTemplate;

    @Test
    public void testRestTemplate() {
        ResponseEntity<String> forEntity = restTemplate.getForEntity(
                "https://www.baidu.com/", String.class);
        System.out.println(forEntity.getStatusCode());
        System.out.println(forEntity.getBody());
    }


    @Test
    public void testRestTemplateGetUser() {

        //String url = "http://localhost:8080/user/info/1";

        String url = "http://localhost:8080/user/info/{1}"; //占位符

        ResponseEntity<JsonResult> forEntity = restTemplate.getForEntity(
                url, JsonResult.class,6);

        if (forEntity != null) {
            HttpStatus statusCode = forEntity.getStatusCode();
            if (HttpStatus.OK.equals(statusCode)) {
                //JsonResult
                JsonResult body = forEntity.getBody();
                // {carId=777} 理论上是UserInfoVO,实际上是 LinkedHashMap
                //data 是json ,实际是排序的k-v LinkedHashMap
                Object data = body.getData();
                System.out.println(data);
                //java.lang.ClassCastException:
                // java.util.LinkedHashMap cannot be cast to
                // cn.tedu.charging.common.pojo.vo.UserInfoVO
                UserInfoVO userInfoVo = (UserInfoVO)data; //强转
                System.out.println(userInfoVo.getCarId());//CarId
            }
        }
    }


    @Test
    public void testRestTemplateExchangeGetUser() {

        //String url = "http://localhost:8080/user/info/1";

        String url = "http://localhost:8080/user/info/{1}"; //占位符

        ParameterizedTypeReference<JsonResult<UserInfoVO>> repType =
                new ParameterizedTypeReference<JsonResult<UserInfoVO>>() {};

        ResponseEntity<JsonResult<UserInfoVO>> forEntity = restTemplate.exchange(
                url, HttpMethod.GET, null, repType, 6);

        if (forEntity != null) {
            HttpStatus statusCode = forEntity.getStatusCode();
            if (HttpStatus.OK.equals(statusCode)) {
                //JsonResult
                JsonResult<UserInfoVO> body = forEntity.getBody();
                // {carId=777} 理论上是UserInfoVO,实际上是 LinkedHashMap
                //data 是json ,实际是排序的k-v LinkedHashMap
                UserInfoVO data = body.getData();
                System.out.println(data);
                //java.lang.ClassCastException:
                // java.util.LinkedHashMap cannot be cast to
                // cn.tedu.charing.common.pojo.vo.UserInfoVO
                //UserInfoVO userInfoVo = (UserInfoVO)data; //无需强转
                System.out.println(data.getCarId());//CarId
            }
        }
    }

    @Test
    public void testRestTemplatePostOrder() {

        String url = "http://localhost:7070/order/create"; //占位符

        OrderAddParam orderAddParam = new OrderAddParam();
        orderAddParam.setUserId(1);
        orderAddParam.setPileId(1);
        orderAddParam.setGunId(1);

        HttpEntity httpEntity = new HttpEntity(orderAddParam);

        ParameterizedTypeReference<JsonResult<String>> repType =
                new ParameterizedTypeReference<JsonResult<String>>() {};

        ResponseEntity<JsonResult<String>> forEntity = restTemplate.exchange(
                url, HttpMethod.POST, httpEntity, repType);

        if (forEntity != null) {
            HttpStatus statusCode = forEntity.getStatusCode();
            if (HttpStatus.OK.equals(statusCode)) {
                JsonResult<String> body = forEntity.getBody();
                String orderNo = body.getData();
                System.out.println(orderNo);
            }
        }
    }

    /**
     * 1 把服务URL 放到数组
     * 2 通过JavaAPI Random 获取随机数
     * 3 把随机数作为数组的下标,去数组通过下标获取URL
     */
    @Test
    public void testRandom() {

        String url = "http://localhost:8080/user/info/{1}";
        String url1 = "http://localhost:8081/user/info/{1}";
        String url2 = "http://localhost:8082/user/info/{1}";

        String[] servers  = new String[3];
        servers[0] = url;
        servers[1] = url1;
        servers[2] = url2;

        // servers[7] 数组越界

        Random random = new Random();

        String server = "";

        for (int i = 0; i < 5 ; i++) {

            int r = random.nextInt(servers.length);

            server = servers[r];

            ParameterizedTypeReference<JsonResult<UserInfoVO>> repType =
                    new ParameterizedTypeReference<JsonResult<UserInfoVO>>() {};

            ResponseEntity<JsonResult<UserInfoVO>> forEntity = restTemplate.exchange(
                    server, HttpMethod.GET, null, repType, 6);

            if (forEntity != null) {
                HttpStatus statusCode = forEntity.getStatusCode();
                if (HttpStatus.OK.equals(statusCode)) {
                    JsonResult<UserInfoVO> body = forEntity.getBody();
                    UserInfoVO data = body.getData();
                    System.out.println(data);
                    System.out.println(data.getCarId());
                }
            }
        }
    }
}
