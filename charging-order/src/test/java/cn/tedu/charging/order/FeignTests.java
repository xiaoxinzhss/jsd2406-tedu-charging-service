package cn.tedu.charging.order;

import cn.tedu.charging.order.feign.DeviceClient;
import cn.tedu.charging.order.feign.UserClient;
import cn.tedu.charing.common.pojo.JsonResult;
import cn.tedu.charing.common.pojo.param.GunStatusUpdateParam;
import cn.tedu.charing.common.pojo.vo.StationInfoVO;
import cn.tedu.charing.common.pojo.vo.UserInfoVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class FeignTests {

    @Autowired
    private UserClient userClient;

    @Autowired
    private DeviceClient deviceClient;

    @Test
    public void testGetStationInfo(){
        JsonResult<StationInfoVO> stationInfo = deviceClient.getStationInfo(1);
        StationInfoVO data = stationInfo.getData();
        System.out.println(data);
    }

    @Test
    public void testUpdateGunStatus(){
        GunStatusUpdateParam param = new GunStatusUpdateParam();
        param.setGunId(1);
        param.setStatus(1);
        JsonResult<Boolean> result = deviceClient.updateGunStatus(param);
        Boolean data = result.getData();
        System.out.println(data);
    }


    @Test
    public void testGetUserInfo() {
        JsonResult<UserInfoVO> userCarInfo = userClient.getUserCarInfo(5);
        UserInfoVO data = userCarInfo.getData();
        System.out.println(data.getCarId());
    }
}
