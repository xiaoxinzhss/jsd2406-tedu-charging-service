package cn.tedu.charging.order;

import cn.tedu.charging.order.common.ChargingConstants;
import cn.tedu.charging.order.mqtt.MqttProducer;
import cn.tedu.charging.order.pojo.dto.ChargingDto;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class MqttTests {

    @Autowired
    private MqttProducer mqttProducer;

    @Test
    public void testSend() {
        String topic = "java";
        String message = "java基础-基本语法-Long";
        mqttProducer.publish(topic,message);
    }

    @Test
    public void testObject() {
        ChargingDto chargingDto = new ChargingDto();
        chargingDto.setOrderNo("1");
        chargingDto.setPileId(1);
        chargingDto.setGunId(1);
        chargingDto.setMsg(ChargingConstants.START_CHARGING);
        System.out.println(chargingDto);
        String string = chargingDto.toString();
        System.out.println(string);
    }



}
