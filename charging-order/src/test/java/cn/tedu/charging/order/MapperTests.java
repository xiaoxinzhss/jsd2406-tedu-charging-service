package cn.tedu.charging.order;

import cn.tedu.charging.order.dao.mapper.OrderSuccessMapper;
import cn.tedu.charging.order.pojo.po.ChargingBillSuccessPO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class MapperTests {

    @Autowired
    private OrderSuccessMapper orderSuccessMapper;

    @Test
    public void testMapper(){
        ChargingBillSuccessPO chargingBillSuccessPO = new ChargingBillSuccessPO();
        chargingBillSuccessPO.setBillId("666");
        orderSuccessMapper.insert(chargingBillSuccessPO);
    }
}
