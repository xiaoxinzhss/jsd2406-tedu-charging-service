package cn.tedu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

//让Spring-boot在启动的时刻 开启FeignClients 支持多个  FeignClient
@EnableFeignClients
@SpringBootApplication
public class ChargingOrderApplication {
    public static void main(String[] args) {
        SpringApplication.run(ChargingOrderApplication.class, args);
    }
}