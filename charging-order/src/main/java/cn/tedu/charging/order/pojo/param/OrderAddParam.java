package cn.tedu.charging.order.pojo.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrderAddParam {

    /**
     * 桩id - 设备信息
     */
    @ApiModelProperty(value = "桩id")
    Integer pileId;

    /**
     * 枪id - 设备信息
     */
    @ApiModelProperty(value = "枪id")
    Integer gunId;

    /**
     * 用户id - 用户信息
     */
    @ApiModelProperty(value = "用户id")
    Integer userId;

}
