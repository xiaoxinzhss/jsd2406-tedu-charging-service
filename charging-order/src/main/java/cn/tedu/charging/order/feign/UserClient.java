package cn.tedu.charging.order.feign;

import cn.tedu.charing.common.pojo.JsonResult;
import cn.tedu.charing.common.pojo.vo.UserInfoVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

//@FeignClient 表示 UserClient 是一个feign的客户端,用来调用别的服务 userService
//指定具体的服务名称 注册中心里的服务名称 userService 表示的是用户服务
//UserClient 是一个访问用户服务的客户端
@FeignClient("userService")
public interface UserClient {

    //@GetMapping 用户服务的接口的地址
    @GetMapping("/user/info/{userId}")
    JsonResult<UserInfoVO> getUserCarInfo(@PathVariable("userId") Integer userId);

}
