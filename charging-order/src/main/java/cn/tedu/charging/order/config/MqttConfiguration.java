package cn.tedu.charging.order.config;

import cn.tedu.charging.order.mqtt.MqttConsumer;
import cn.tedu.charging.order.mqtt.MqttContext;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Random;

/**
 * EMQX消息中间件 的连接配置
 */
@Configuration
public class MqttConfiguration {

    @Autowired
    private MqttContext mqttContext;

    private static final Logger log = LoggerFactory.getLogger(MqttConfiguration.class);
    /**
     * 用户名
     * 这些配置信息 应该放到配置文件, 或者放到配置中心 nacos (注册中心和配置中心)
     */
    private String userName = "admin";

    /**
     * 密码
     */
    private String password = "public";

    /**
     * 连接地址 tcp协议 端口 1883
     * mysql jdbc://localhost:3306/databaseName
     */
    private String host = "tcp://localhost:1883";

    /**
     * 客户端 启动多个订单服务,需要保证每个订单服务的clientId 不同
     */
    private String clientId = "order_service_client" + getRandom();

    private Integer getRandom(){
        Random random = new Random();
        return random.nextInt(100);
    }

    @Bean
    public MqttClient mqttClient() throws MqttException {
        /**
         * 通过连接地址和clientId 进行连接
         */
        MqttClient mqttClient = new MqttClient(host, clientId);

        /**
         * 连接相关的配置
         */
        MqttConnectOptions connOpts = new MqttConnectOptions();
        connOpts.setUserName(userName);
        connOpts.setPassword(password.toCharArray());
        //清理会话session
        connOpts.setCleanSession(true);
        //自动重连
        connOpts.setAutomaticReconnect(true);
        //版本号设置
        connOpts.setMqttVersion(MqttConnectOptions.MQTT_VERSION_3_1_1);

        //mqttConsumer 是通过new 的方式创建的,没有在spring容器里
        //MqttClient 是通过@Bean的方式 创建的,在spring容器里
        //没有在spring容器里的mqttConsumer 想使用 在spring容器里 MqttClient
        MqttConsumer mqttConsumer = new MqttConsumer(mqttContext,mqttClient);
        //通过写方法设置mqttClient
        //mqttConsumer.setMqttClient(mqttClient);
        mqttClient.setCallback(mqttConsumer);
        //connect mqttClient的连接方法,基于配置 connOpts 和 EMQX 进行连接
        mqttClient.connect(connOpts);
        log.debug("MqttConfiguration#mqttClient,设置了回调类后,和EMQX进行连接");
        return mqttClient;
    }

}
