package cn.tedu.charging.order.dao.repository.impl;

import cn.tedu.charging.order.common.OrderStatusConstant;
import cn.tedu.charging.order.dao.mapper.OrderFailMapper;
import cn.tedu.charging.order.dao.mapper.OrderSuccessMapper;
import cn.tedu.charging.order.dao.repository.OrderRepository;
import cn.tedu.charging.order.pojo.po.ChargingBillFailPO;
import cn.tedu.charging.order.pojo.po.ChargingBillSuccessPO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("orderRepository")
public class OrderRepositoryImpl implements OrderRepository {

    @Autowired
    private OrderSuccessMapper orderSuccessMapper;

    @Autowired
    private OrderFailMapper orderFailMapper;

    @Override
    public Integer updateOrderStatusFromProcessToExceptionEnd(String orderNo) {
        // update xxx set bill_status = 异常结束  where bill_id = 订单号 and bill_status =  充电中
        //更新目标值 bill_status = 异常结束   entity
        ChargingBillSuccessPO chargingBillSuccessPO = new ChargingBillSuccessPO();
        chargingBillSuccessPO.setBillStatus(OrderStatusConstant.ORDER_STATUS_EXCEPTION_END);
        //更新的条件 UpdateWrapper
        UpdateWrapper wrapper = new UpdateWrapper<>();
        //bill_id = orderNo
        wrapper.eq("bill_id",orderNo);
        //bill_status = 充电中
        wrapper.eq("bill_status",OrderStatusConstant.ORDER_STATUS_PROCESS);
        return orderSuccessMapper.update(chargingBillSuccessPO,wrapper);
    }

    @Override
    public ChargingBillSuccessPO getSuccessOrder(String orderNo) {
        QueryWrapper<ChargingBillSuccessPO> wrapper = new QueryWrapper<>();
        wrapper.eq("bill_id",orderNo);
        return orderSuccessMapper.selectOne(wrapper);
    }

    @Override
    public ChargingBillFailPO getFailOrder(String orderNo) {
        QueryWrapper<ChargingBillFailPO> wrapper = new QueryWrapper<>();
        wrapper.eq("bill_id",orderNo);
        return orderFailMapper.selectOne(wrapper);
    }

    @Override
    public Integer saveFailOrder(ChargingBillFailPO newChargingBillFailPO) {
        return orderFailMapper.insert(newChargingBillFailPO);
    }

    @Override
    public Integer updateDeviceInfo(String orderNo, Integer gunId) {
        // update xxx set gunId = 枪id  where bill_id = 订单号
        //更新目标值 gunId = 枪id   entity
        ChargingBillSuccessPO chargingBillSuccessPO = new ChargingBillSuccessPO();
        chargingBillSuccessPO.setGunId(gunId);
        //更新的条件 UpdateWrapper
        UpdateWrapper wrapper = new UpdateWrapper<>();
        //bill_id = orderNo
        wrapper.eq("bill_id",orderNo);
        return orderSuccessMapper.update(chargingBillSuccessPO,wrapper);
    }
}
