package cn.tedu.charging.order.common;

public class MqttConstants {

    /**
     * 订单给设备发送 开始充电 指令的 topic 前缀
     */
    public static final String TOPIC_START_CHARGING_PREFIX = "/topic/start/";


    /**
     * 设备给订单发送 开始充电结果  topic
     */
    public static final String TOPIC_CHARGING_RESULT = "/topic/charging/result";

    /**
     * 设备给订单发送 充电进度的topic
     */
    public static final String TOPIC_CHARGING_PROCESS = "/topic/charging/process";


}
