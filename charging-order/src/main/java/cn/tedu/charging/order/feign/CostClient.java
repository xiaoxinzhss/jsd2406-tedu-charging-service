package cn.tedu.charging.order.feign;

import cn.tedu.charing.common.pojo.JsonResult;
import cn.tedu.charing.common.pojo.param.ChargingProcessParam;
import cn.tedu.charing.common.pojo.vo.ChargingProcessVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("costService")
public interface CostClient {

    @PostMapping("/cost/calculate")
    JsonResult<ChargingProcessVO> calculateCost(@RequestBody ChargingProcessParam chargingProcessParam);

}
