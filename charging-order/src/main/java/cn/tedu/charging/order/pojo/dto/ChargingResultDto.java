package cn.tedu.charging.order.pojo.dto;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

/**
 * 设备接收到订单发送的开始充电指令后
 * 设备给订单服务返回的响应
 *  设备的响应 两种响应
 *    1 开始充电成功 ,设备收到指令后,证明设备能收消息 能充电
 *    2 开始充电失败 ,设备收到指令后,证明设备能收消息 不能充电
 */
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ChargingResultDto {

    /**
     * 订单号
     */
    String orderNo;

    /**
     * 返回结果
     */
    String result;

}
