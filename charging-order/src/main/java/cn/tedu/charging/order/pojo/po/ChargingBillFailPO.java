package cn.tedu.charging.order.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@TableName("charging_bill_fail")
public class ChargingBillFailPO {
    /**
     * 订单信息 id
     */
    @TableId(type = IdType.AUTO)
    Integer id;

    /**
     * 订单信息 订单编号
     */
    String billId;


    /**
     * 失败原因
     */
    String failDesc;


    /**
     * 用户信息 id
     */
    Integer userId;

    /**
     * 设备信息 枪id 类似与电商里的 商品id
     */
    Integer gunId;
}
