package cn.tedu.charging.order.pojo.dto;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

/**
 * 同步充电进度
 */
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ChargingProcessDto {

    /**
     * 订单编号 订单相关
     */
    String orderNo;

    /**
     * 站id 设备相关
     */
    Integer stationId;

    /**
     * 枪id 设备相关
     */
    Integer gunId;

    /**
     * 用户id 用户相关
     */
    Integer userId;


    /**
     * 电量 充了多少度电 设备的实时状态
     */
    Float chargingCapacity;

    /**
     * 温度  设备的实时状态
     */
    Float temperature;


    /**
     * 是否充满 充电设备知道
     */
    Boolean isFull;
}
