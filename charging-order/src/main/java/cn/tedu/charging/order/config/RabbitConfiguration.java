package cn.tedu.charging.order.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 *  思考 RabbitConfiguration 配置连接信息 没有配置连接信息
 */
@Configuration
public class RabbitConfiguration {

    public static final String EXCHANGE_NAME = "charging_order_exchange";

    private static final String QUEUE_NAME = "charging_order_queue";

    public static final String ROUTING_KEY = "charging_order_routing_key";


    private static final String DEAD_LETTER_EXCHANGE_NAME = "dead_letter_charging_order_exchange";

    public static final String DEAD_LETTER_QUEUE_NAME = "dead_letter_charging_order_queue";

    private static final String DEAD_LETTER_ROUTING_KEY = "dead_letter_charging_order_routing_key";


    @Bean
    public DirectExchange orderExchange(){
        //消息持久化,
        Boolean durable = true;
        //自动删除 一次性
        Boolean autoDel = false;
        return new DirectExchange(EXCHANGE_NAME,durable,autoDel);
    }

    @Bean
    public Queue orderQueue(){
        Map<String,Object> args = new HashMap<>();

        //设置消息的TTL 存活时间 模拟充电充满需要 2 分钟
        Integer ttl = 2 * 60 * 1000;
        args.put("x-message-ttl",ttl);

        //设置queue的死信Exchange
        args.put("x-dead-letter-exchange",DEAD_LETTER_EXCHANGE_NAME);
        //设置queue和死信Exchange 绑定 routing_key
        args.put("x-dead-letter-routing-key",DEAD_LETTER_ROUTING_KEY);

        //惰性队列,在消息很多的时候,把消息存储到磁盘,避免消息积压,占用内存
        args.put("x-queue-mode","lazy");

        //消息持久化,
        Boolean durable = true;
        //自动删除 一次性
        Boolean autoDel = false;
        return new Queue(QUEUE_NAME,durable,true,autoDel,args);
    }

    /**
     * 把queue和Exchange通过routing-key绑定
     * @return
     */
    @Bean
    public Binding orderBinding(){
        return BindingBuilder.bind(orderQueue()).to(orderExchange()).with(ROUTING_KEY);
    }

    /**
     * 定义死信  Exchange
     * @return
     */
    @Bean
    public DirectExchange deadLetterOrderExchange(){
        //消息持久化,
        Boolean durable = true;
        //自动删除 一次性
        Boolean autoDel = false;
        return new DirectExchange(DEAD_LETTER_EXCHANGE_NAME,durable,autoDel);
    }

    /**
     * 定义订单死信Queue
     * @return
     */
    @Bean
    public Queue deadLetterOrderQueue(){
        //消息持久化 消息不能丢失
        Boolean durable = true;
        return new Queue(DEAD_LETTER_QUEUE_NAME,durable);
    }


    /**
     * 把死信queue和死信Exchange通过死信routing-key进行绑定
     * @return
     */
    @Bean
    public Binding deadLetterOrderBinding(){
        return BindingBuilder.
                bind(deadLetterOrderQueue()).
                to(deadLetterOrderExchange()).
                with(DEAD_LETTER_ROUTING_KEY);
    }





}
