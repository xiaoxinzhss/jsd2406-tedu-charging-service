package cn.tedu.charging.order.pojo.po;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.math.BigDecimal;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Document(indexName = "charging_process_2406")
public class ChargingProcessPO {

    @Id
    Long id;

    /**
     * 订单编号 订单相关
     */
    String orderNo;

    /**
     * 站id 设备相关
     */
    Integer stationId;

    /**
     * 枪id 设备相关
     */
    Integer gunId;

    /**
     * 用户id 用户相关
     */
    Integer userId;


    /**
     * 电量 充了多少度电 设备的实时状态
     */
    Float chargingCapacity;

    /**
     * 花费 订单的实时花费
     */
    BigDecimal totalCost;

    /**
     * 温度  设备的实时状态
     */
    Float temperature;


    /**
     * 是否充满 充电设备知道
     */
    Boolean isFull;
}
