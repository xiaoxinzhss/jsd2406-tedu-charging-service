package cn.tedu.charging.order.dao.repository;

import cn.tedu.charging.order.pojo.po.ChargingBillFailPO;
import cn.tedu.charging.order.pojo.po.ChargingBillSuccessPO;

public interface OrderRepository {

    Integer updateOrderStatusFromProcessToExceptionEnd(String orderNo);

    ChargingBillSuccessPO getSuccessOrder(String orderNo);

    ChargingBillFailPO getFailOrder(String orderNo);

    Integer saveFailOrder(ChargingBillFailPO newChargingBillFailPO);

    Integer updateDeviceInfo(String orderNo, Integer gunId);
}
