package cn.tedu.charging.order.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
//指定 ChargingBillSuccessPO要映射的表 charging_bill_success ORM
@TableName("charging_bill_success")
public class ChargingBillSuccessPO {
    /**
     * 订单信息 id
     * AUTO 数据库ID自增
     */
    @TableId(type = IdType.AUTO)
    Integer id;

    /**
     * 订单信息 订单编号
     */
    String billId;

    /**
     * 订单信息 订单状态
     */
    Integer billStatus;


    /**
     * 用户信息 id
     */
    Integer userId;

    /**
     * 设备信息 枪id 类似与电商里的 商品id
     */
    Integer gunId;
}
