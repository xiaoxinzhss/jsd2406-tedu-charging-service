package cn.tedu.charging.order.common;

/**
 * 常量类
 */
public class ChargingConstants {

    /**
     * 表示枪被使用 常量名称 一定要有业务含义
     */
    public static final Integer GUN_STATUS_BUSY = 2;

    /**
     * 订单的服务给设备发送的  开始充电指令
     */
    public static final String START_CHARGING = "start_charging";


    /**
     * 设备返回开始充电结果 开始充电成功
     */
    public static final String RESULT_START_CHARGING_SUCCESS = "start_success";

}
