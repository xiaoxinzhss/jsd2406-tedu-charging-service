package cn.tedu.charging.order.rabbit;

import cn.tedu.charging.order.config.RabbitConfiguration;
import cn.tedu.charging.order.dao.repository.OrderRepository;
//import cn.tedu.charging.order.dao.mapper.OrderSuccessMapper;
import cn.tedu.charging.order.pojo.po.OrderMQPO;
//import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;


@Slf4j
@Component
public class RabbitMQOrderDeadLetterConsumer {

    //@Autowired
    //private OrderSuccessMapper orderSuccessMapper;

    @Autowired
    private OrderRepository orderRepository;

    /**
     * 消费死信队列消息
     * 通过订单号 去成功订单表查询 订单的状态
     * 如果订单状态 是 充电中 说明 在规定的时间内还没充满 订单状态没有变成 充电完成 异常订单 需要把订单状态 改为 异常结束
     * 如果订单状态 是 充电完成 说明 在规定的时间内已经完成 订单状态是 充电完成 正常订单 无需
     * @param orderMQPO
     * // 通过RabbitMQ 里的死信队列实现延迟消息,处理充电订单里的超时订单
     */
    @RabbitListener(queues = RabbitConfiguration.DEAD_LETTER_QUEUE_NAME)
    public void consumeChargingQueue(OrderMQPO orderMQPO, Message message, Channel channel) throws IOException {
        log.debug("消息死信消息:msg:{}", orderMQPO);

        orderRepository.updateOrderStatusFromProcessToExceptionEnd(orderMQPO.getOrderNo());
        log.debug("消息死信消息:msg:{},修改订单状态,如果是充电中修改为异常结束," +
                "如果是充电完成,无需处理", orderMQPO);



        //消息队列中的ack
        //RabbitMQOrderDeadLetterConsumer 消息者 通过 consumeChargingQueue
        //消费死信队列 DEAD_LETTER_QUEUE_NAME,处理业务,更新订单状态

        // 正常情况 自动ack 默认
        // 更新订单状态 updateOrderStatusFromProcessToExceptionEnd
        // 消费者自动 给 RabbitMQ 返回一个响应 ack 消费者告诉RabbitMQ 消息消费成功了
        // RabbitMQ 收到ack后,会认为消费者消费成功了,RabbitMQ会把这条消息从队列中删除

        // 异常情况 更新订单状态,数据库出现异常,更新失败,消费者抛异常,
        // 如果是自动ack 消费者 自动(不管消费者是否有异常) 给 RabbitMQ 返回一个响应 ack
        // RabbitMQ 收到ack后,会认为消费者消费成功了,RabbitMQ会把这条消息从队列中删除

        //消息没有被成功的消费,而且还被RabbitMQ 删除 消息丢失了,超时订单没有被正确的处理

        //手动ack 需要通过代码的方式 告诉 rabbitMq
        //配置文件ack 改为手动方式
        //spring.rabbitmq.listener.direct.acknowledge-mode=manual
        //把下面的两行代码注释掉,updateOrderStatusFromProcessToExceptionEnd执行完
        //消息会一直在rabbitMQ的队列里,不会被删除
        long deliveryTag = message.getMessageProperties().getDeliveryTag();
        channel.basicAck(deliveryTag,true);
    }
}