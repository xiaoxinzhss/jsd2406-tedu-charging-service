package cn.tedu.charging.order.feign;

import cn.tedu.charing.common.pojo.JsonResult;
import cn.tedu.charing.common.pojo.param.GunStatusUpdateParam;
import cn.tedu.charing.common.pojo.vo.StationInfoVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

//@FeignClient 表示 DeviceClient 是一个feign的客户端,用来调用别的服务 deviceService
//指定具体的服务名称 注册中心里的服务名称 deviceService 表示的是设备服务
//UserClient 是一个访问用户服务的客户端
@FeignClient("deviceService")
public interface DeviceClient {

    @GetMapping("/device/station/info/{gunId}")
    JsonResult<StationInfoVO> getStationInfo(@PathVariable("gunId") Integer gunId);

    @PostMapping("/device/station/gun/status/update")
    JsonResult<Boolean> updateGunStatus(@RequestBody GunStatusUpdateParam param);

}
