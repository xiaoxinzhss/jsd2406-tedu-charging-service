package cn.tedu.charging.order.controller;



import cn.tedu.charging.order.pojo.param.OrderAddParam;
import cn.tedu.charging.order.service.OrderService;
import cn.tedu.charing.common.pojo.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/order")
@Api(tags = "订单接口")
public class OrderController {

    /**
     * 创建订单
     * Dto 数据传输对象 前端把数据传输给了后端的OrderController
     * 服务之间调用 RPC 远程过程调用 Dto
     * Param 前端调用后端   OrderAddParam OrderUpdateParam 解耦
     *
     * RESTFul 接口定义的规范 希望你的接口调用方式按照 http的请求方式 POST GET DELETE PUT
     * R 资源
     * 查询 GET
     * 增加 PUT
     * 更新 POST
     * 删除 DELETE
     *
     * 调用方法会退化 只用 GET POST
     * 查询 GET
     * 增加 更新 删除 POST
     *  createOrder
     *  OrderCreate
     *  资源/动作   或者  动作/资源
     * C 创建订单 /order/create                   /create/order/
     * R 获取订单详情 /order/info/{orderNo}
     * R 获取订单列表 /order/list/{userId}
     * R 通过条件查询订单列表 /order/search/ 订单号,订单的开始时间,结束时间,订单的状态
     *   多个的入参 OrderQueryParam   OrderSearchQuery
     * D 删除订单  /order/del/{orderNo}
     * U 更新订单 /order/update
     *
     * @return
     */

    @Autowired
    private OrderService orderService;


    @ApiOperation("创建订单")
    @PostMapping("create")
    public JsonResult<String> creatOrder(@RequestBody OrderAddParam orderAddParam){
        //创建类型的接口,是否要返回出参,要根据业务来决定,
        //如果后续的业务需要基于当前的接口的出参,做其他的操作,
        //比如 通过返回的出参(订单号),去查看订单详情,出参(订单号)会作为订单详情入参
        String orderNo = orderService.createOrder(orderAddParam);
        JsonResult<String> result = new JsonResult<>();
        result.setCode(2000);
        result.setMsg("订单创建成功");//可选
        result.setData(orderNo);
        return result;
    }
}
