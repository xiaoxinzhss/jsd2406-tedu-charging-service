package cn.tedu.charging.order.mqtt;

import cn.tedu.charging.order.dao.mapper.OrderFailMapper;
import cn.tedu.charging.order.dao.mapper.OrderSuccessMapper;
import cn.tedu.charging.order.dao.repository.ChargingProcessESRepository;
import cn.tedu.charging.order.feign.CostClient;
import cn.tedu.charging.order.websocket.WebSocketServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MqttContext {

    @Autowired
    private OrderSuccessMapper orderSuccessMapper;

    @Autowired
    private OrderFailMapper orderFailMapper;

    @Autowired
    private CostClient costClient;

    @Autowired
    private WebSocketServer webSocketServer;

    @Autowired
    private ChargingProcessESRepository chargingProcessESRepository;

    public ChargingProcessESRepository getChargingProcessESRepository() {
        return chargingProcessESRepository;
    }

    public WebSocketServer getWebSocketServer() {
        return webSocketServer;
    }

    public CostClient getCostClient() {
        return costClient;
    }

    public OrderSuccessMapper getOrderSuccessMapper() {
        return orderSuccessMapper;
    }

    public OrderFailMapper getOrderFailMapper() {
        return orderFailMapper;
    }
}
