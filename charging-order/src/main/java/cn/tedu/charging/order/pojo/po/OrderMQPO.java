package cn.tedu.charging.order.pojo.po;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

/**
 * 订单服务和MQ传输的数据对象 用DTO 可以
 * 订单服务发送给MQ MQ需要存储消息 PO 可以
 *
 * 需要实现序列化
 *  implements Serializable
 *
 *  接口没有任何的方法 叫做  标记接口
 *
 */
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrderMQPO implements Serializable {

    /**
     * 订单编号
     */
    String orderNo;

}
