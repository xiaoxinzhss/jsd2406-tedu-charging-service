package cn.tedu.charging.order.dao.repository;

import cn.tedu.charging.order.pojo.po.ChargingProcessPO;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface ChargingProcessESRepository
        extends ElasticsearchRepository<ChargingProcessPO,Long> {
}
