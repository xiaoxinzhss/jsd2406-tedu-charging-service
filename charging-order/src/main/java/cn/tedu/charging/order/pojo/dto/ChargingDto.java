package cn.tedu.charging.order.pojo.dto;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

/**
 * 订单服务和设备之间的传输数据对象
 *
 * 订单服务 开始充电 指令  的 目的是 告诉设备  请你  用哪个设备(枪id搞定) 帮 它(订单号搞定)  开始充电
 */
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ChargingDto {

    /**
     * 订单号
     */
    String orderNo;


    /**
     * 桩id
     */
    Integer pileId;

    /**
     * 枪id
     */
    Integer gunId;

    /**
     *  指令
     *  1 开始充电
     *  2 结束充电
     */
    String msg;
}
