package cn.tedu.charging.order.common;

public class OrderStatusConstant {

    /**
     * 充电中
     */
    public static final Integer ORDER_STATUS_PROCESS = 1;

    /**
     * 充电完成 正常结束
     */
    public static final Integer ORDER_STATUS_NORMAL_END = 2;


    /**
     * 异常结束
     */
    public static final Integer ORDER_STATUS_EXCEPTION_END = 3;
}
