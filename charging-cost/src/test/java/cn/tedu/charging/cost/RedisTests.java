package cn.tedu.charging.cost;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

import javax.sql.DataSource;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;

@SpringBootTest
public class RedisTests {

    @Autowired
    private RedisTemplate<String, Serializable> redisTemplate;

    @Autowired
    private DataSource dataSource;

    @Test
    public void test() throws SQLException {
        RedisConnectionFactory connectionFactory = redisTemplate.getConnectionFactory();
        RedisConnection connection = connectionFactory.getConnection();
        /**
         * Jedis是Redis的一个广泛使用的Java客户端，JedisPool是Jedis提供的连接池实现。通过配置JedisPool，可以有效地管理Redis连接，避免频繁创建和销毁连接带来的性能开销‌12。
         * Redisson是一个在Redis的基础上实现了分布式和可扩展的Java数据结构。它不仅提供了多种分布式数据结构，还内置了连接池管理功能，使得使用Redis变得更加方便‌1。
         * Lettuce是一个异步的、基于Netty的Redis客户端，支持同步、异步和响应式模式。Lettuce的连接池管理是通过Netty的连接池实现的，它支持线程安全的连接共享，通常不需要显式配置连接池，但在需要时也可以配置‌1。
         */
        System.out.println("Redis的连接" + connection);

        String k1 = "k1";
        //String v1 = "v1";
        //connection.set(k1.getBytes(), v1.getBytes());

        //byte[] bytes = connection.get(k1.getBytes());
       // System.out.println(new String(bytes));

        Serializable serializable =  redisTemplate.opsForValue().get(k1);
        System.out.println(serializable);


        Connection connection1 = dataSource.getConnection();
        //https://blog.csdn.net/2302_77874940/article/details/135058614
        System.out.println("数据库的连接" + connection1);

    }




}
