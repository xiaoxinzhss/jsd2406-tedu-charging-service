package cn.tedu.charging.cost.dao.repository;

import cn.tedu.charging.cost.pojo.po.ChargingDataPO;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface ChargingDataESRepository extends ElasticsearchRepository<ChargingDataPO,String> {
}
