package cn.tedu.charging.cost.dao.repository;

import cn.tedu.charging.cost.pojo.po.CostRulePO;

import java.util.List;

public interface CostRuleCacheRepository {

    List<CostRulePO> getCostRuleByStationId(Integer stationId);

    void saveCostRules(Integer stationId,List<CostRulePO> dbCostRules);
}
