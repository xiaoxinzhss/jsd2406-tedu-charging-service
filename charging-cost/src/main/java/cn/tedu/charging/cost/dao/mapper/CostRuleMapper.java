package cn.tedu.charging.cost.dao.mapper;

import cn.tedu.charging.cost.pojo.po.CostRulePO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CostRuleMapper extends BaseMapper<CostRulePO> {
}
