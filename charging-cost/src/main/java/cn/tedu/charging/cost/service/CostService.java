package cn.tedu.charging.cost.service;

import cn.tedu.charing.common.pojo.param.ChargingProcessParam;
import cn.tedu.charing.common.pojo.vo.ChargingProcessVO;

public interface CostService {
    ChargingProcessVO calculateCost(ChargingProcessParam chargingProcessParam);
}
