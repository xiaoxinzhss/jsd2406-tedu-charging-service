package cn.tedu.charging.cost.pojo.po;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@TableName("charging_cost_rule")
public class CostRulePO implements Serializable {
    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    Integer id;

    /**
     * 站id
     */
    Integer stationId;

    /**
     * 枪类型
     */
    Integer gunType;

    /**
     * 名称
     */
    String name;

    /**
     * 开始时间
     * 8 - 12
     * 8:10 - 9:10
     */
    Integer startTime;

    /**
     * 结束时间
     */
    Integer endTime;

    /**
     * 价格 浮点
     * Long 保证全公司的业务系统的价格计算 要统一 保留位数
     * 10.51(页面)  10.51*100 = 1051(数据库)
     * 10.511(页面)  10.51*1000 = 10511(数据库)
     */
    BigDecimal powerFee;

    /**
     * 服务费
     */
    BigDecimal serviceFee;

}
