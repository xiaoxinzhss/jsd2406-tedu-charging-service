package cn.tedu.charging.cost.dao.repository.impl;

import cn.tedu.charging.cost.dao.repository.CostRuleCacheRepository;
import cn.tedu.charging.cost.pojo.po.CostRulePO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;

@Repository
public class CostRuleCacheRepositoryImpl implements CostRuleCacheRepository {

    @Autowired
    private RedisTemplate<String, List<CostRulePO>> redisTemplate;

    @Override
    public List<CostRulePO> getCostRuleByStationId(Integer stationId) {
        ValueOperations<String, List<CostRulePO>> stringOperations = redisTemplate.opsForValue();
        String costRuleKey = "COST_RULE_KEY_" + stationId;
        return stringOperations.get(costRuleKey);
    }

    @Override
    public void saveCostRules(Integer stationId, List<CostRulePO> dbCostRules) {
        String costRuleKey = "COST_RULE_KEY_" + stationId;
        redisTemplate.opsForValue().set(costRuleKey, dbCostRules);
    }
}
