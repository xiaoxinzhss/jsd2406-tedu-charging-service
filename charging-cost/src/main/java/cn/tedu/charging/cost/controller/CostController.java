package cn.tedu.charging.cost.controller;

import cn.tedu.charing.common.pojo.param.ChargingProcessParam;
import cn.tedu.charing.common.pojo.vo.ChargingProcessVO;
import cn.tedu.charging.cost.service.CostService;
import cn.tedu.charing.common.pojo.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("cost")
@Api(tags = "计价服务")
public class CostController {


    @Autowired
    private CostService costService;


    @ApiOperation("计算电费")
    @PostMapping("calculate")
    public JsonResult<ChargingProcessVO> calculateCost(@RequestBody ChargingProcessParam chargingProcessParam) {
        ChargingProcessVO chargingProcessVO =
                costService.calculateCost(chargingProcessParam);
        return JsonResult.ok(chargingProcessVO);
    }
}
