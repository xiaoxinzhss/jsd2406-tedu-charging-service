package cn.tedu.charging.cost.dao.repository.impl;

import cn.tedu.charging.cost.dao.mapper.CostRuleMapper;
import cn.tedu.charging.cost.dao.repository.CostRuleRepository;
import cn.tedu.charging.cost.pojo.po.CostRulePO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;

@Repository
public class CostRuleRepositoryImpl implements CostRuleRepository {

    @Autowired
    private CostRuleMapper costRuleMapper;

    @Override
    public List<CostRulePO> getCostRules(Integer stationId) {
        QueryWrapper<CostRulePO> queryWrapper = new QueryWrapper<CostRulePO>();
        queryWrapper.eq("station_id", stationId);
        return costRuleMapper.selectList(queryWrapper);
    }
}
