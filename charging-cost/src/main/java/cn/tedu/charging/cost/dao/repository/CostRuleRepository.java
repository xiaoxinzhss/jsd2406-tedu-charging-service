package cn.tedu.charging.cost.dao.repository;

import cn.tedu.charging.cost.pojo.po.CostRulePO;

import java.util.List;

public interface CostRuleRepository {
    List<CostRulePO> getCostRules(Integer stationId);
}
