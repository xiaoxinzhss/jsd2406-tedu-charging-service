package cn.tedu.charging.cost.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

@Configuration
public class RedisConfiguration {

    @Bean
    public RedisTemplate redisTemplate(RedisConnectionFactory connectionFactory) {
        RedisTemplate redisTemplate = new RedisTemplate();
        //设置连接工厂
        redisTemplate.setConnectionFactory(connectionFactory);
        //设置key的序列化器 redis的key 字符串
        redisTemplate.setKeySerializer(RedisSerializer.string());
        //设置key的序列化器 redis的value Json
        redisTemplate.setValueSerializer(RedisSerializer.json());
        return redisTemplate;
    }
}
