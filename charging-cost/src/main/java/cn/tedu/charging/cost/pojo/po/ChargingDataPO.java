package cn.tedu.charging.cost.pojo.po;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.math.BigDecimal;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Document(indexName = "charging_data_2406")
public class ChargingDataPO {

    @Id
    String id;

    /**
     * 订单号
     */
    String orderNo;

    /**
     * 设备信息 枪id
     */
    Integer gunId;

    /**
     * 用户信息 用户id
     */
    Integer userId;

    /**
     * 开始时间 第一次同步数据的时间
     */
    Long startTime;


    /**
     * 花费
     */
    BigDecimal totalCost;

    /**
     * 充电度数
     */
    Float chargingCapacity;


    /**
     * 同步次数
     */
    Integer count;

    /**
     * 判断同步的充电进度是否是第一次
     * 充血模型 在数据类 里增加了业务方法
     * 反之 贫血模型
     * DDD 领域驱动开发
     * @return
     */
    public Boolean isFirst(){
        return count == 1;
    }

    /**
     * 通过充血模型 计算真实充电度数
     * @param chargingCapacity
     * @return
     */
    public Float getChargingCapacity(Float chargingCapacity) {
        if (isFirst()) {
            return chargingCapacity;
        }else {
            return chargingCapacity - this.chargingCapacity;
        }
    }

    /**
     * 通过充血模型 计算总花费
     * 好处 是把计算花费的业务逻辑 内聚 到 ChargingData  高内聚 松耦合
     * 适合于复杂的业务场景
     *  //上次花费
     *  BigDecimal lastTotalCost = chargingData.getTotalCost();
     *  //当前总花费 =  上次花费 + 本次花费
     *  BigDecimal totalCost = lastTotalCost.add(currentCost);
     *  //保存当前总花费到充电数据  ChargingData
     *  chargingData.setTotalCost(totalCost);
     * @param totalCost
     */
    public void  setTotalCost(BigDecimal totalCost){
        if(isFirst()) {
            this.totalCost = totalCost;
        }else {
            this.totalCost = this.totalCost.add(totalCost);
        }
    }
}
