# JSD2406-tedu-charging-service

#### 介绍
{**以下是 Gitee 平台说明，您可以替换此简介**
Gitee 是 OSCHINA 推出的基于 Git 的代码托管平台（同时支持 SVN）。专为开发者提供稳定、高效、安全的云端软件开发协作平台
无论是个人、团队、或是企业，都能够用 Gitee 实现代码托管、项目管理、协作开发。企业项目请看 [https://gitee.com/enterprises](https://gitee.com/enterprises)}

#### 软件架构
软件架构说明


#### 安装教程
Nacos  startup.cmd -m standalone  注册中心
Emqx   emqx start              IOT的消息中间件
ES     elasticsearch.bat       时序数据的存储/计价价格累加存储
Redis  redis-server.exe        计价规则/场站位置/场站基本信息 存储
RabbitMQ  开机自启              死信队列处理超时订单处理,消息通知,服务之间通信      
MySql     开机自启              数据存储/订单/用户/设备/运营商/计价规则
Canal  startup.bat             数据库场站信息和位置信息和Redis保持一致

charging-gateway 网关 前端的入口
charging-device  设备服务  附近的充电站/充电站详情/设备详情
charging-order   订单服务  创建订单/订单详情/我的订单
charging-user    用户服务  登录/注册/用户信息修改/查看/充值/绑定车辆
charging-cost    计价服务  价格计算







#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
