package cn.tedu.charing.common.pojo.vo;


import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserInfoVO {

    /**
     * 车辆id
     * 暂时只允许一个用户绑定一个车辆
     * 后续如果绑定多个车辆
     * List<Integer> carIds
     */
    Integer carId;

}
