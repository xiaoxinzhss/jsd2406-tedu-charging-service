package cn.tedu.charing.common.pojo.param;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class GunStatusUpdateParam {

    /**
     * 枪id
     */
    Integer gunId;

    /**
     * 目标状态
     */
    Integer status;

}
