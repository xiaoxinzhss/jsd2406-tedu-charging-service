package cn.tedu.charing.common.pojo.vo;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class StationInfoVO {

    Integer stationId;

    /**
     * 场站名称
     */
    String stationName;

    /**
     * 距离
     */
    Double distance;

    /**
     * 经度 位置信息
     */
    BigDecimal stationLng;

    /**
     * 维度 位置信息
     */
    BigDecimal stationLat;
}
