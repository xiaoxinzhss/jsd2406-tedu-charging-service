package cn.tedu.charing.common.pojo;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class JsonResult<T> {

    /**
     * 状态码
     */
    Integer code;

    /**
     * 消息
     */
    String msg;

    /**
     * 接口的出参,具体的数据
     * type T
     */
    T data;


    public static JsonResult ok(Object data,String msg) {
        JsonResult result = new JsonResult<>();
        result.setCode(2000);
        result.setMsg(msg);//可选
        result.setData(data);
        return result;
    }

    public static JsonResult ok(Object data) {
        return ok(data,null);
    }
}
