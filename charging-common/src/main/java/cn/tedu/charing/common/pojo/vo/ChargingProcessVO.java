package cn.tedu.charing.common.pojo.vo;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;

/**
 * 理解怎么梳理出参
 * 页面要显示什么?
 * 客户 (页面,接口调用者)更关心什么数据,接口的出参就返回什么数据
 */
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ChargingProcessVO {

    /**
     * 订单号 订单信息
     */
    String orderNo;

    /**
     * 用户id 用户信息
     */
    Integer userId;

    /**
     * 总金额 订单信息
     */
    BigDecimal totalCost;


    /**
     * 充电时长
     * 返回什么格式
     * 数据库要存储 通用的格式
     * 返回的也是通用的格式
     * 具体的格式转换 交给 前端
     * 数据库 存储 时间戳
     * 前端A xx小时-xx分钟-xx秒
     * 前端B xx消息.xx分钟.xx秒
     */
    String totalTime;

    /**
     * 总度数 订单信息
     */
    Float chargingCapacity;

    /**
     * 枪id
     */
    Integer gunId;

}
