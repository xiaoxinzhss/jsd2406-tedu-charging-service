package cn.tedu.charing.common.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Json工具类
 * 对象转换为Json
 * Json 转对象
 */
public class JsonUtils {

    /**
     * 定义对象转换工具 SpringBoot默认 jackson ObjectMapper
     * static 表示静态 jvm在加载的时候 会创建 还能保证只创建一个
     */
    private static  ObjectMapper mapper = new ObjectMapper();

    /**
     * java对象转换为Json
     * @param object
     * @return
     */
    public static String toJson(Object object) {
        // 没有必要每次调用toJson 都创建  ObjectMapper
        //占用jvm的内存空间,可以搞成单例
        //ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> T fromJson(String json,Class<T> clazz) {
        try {
            return mapper.readValue(json, clazz);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}

