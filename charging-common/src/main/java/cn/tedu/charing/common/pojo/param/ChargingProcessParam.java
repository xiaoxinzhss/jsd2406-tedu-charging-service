package cn.tedu.charing.common.pojo.param;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

/**
 * 理解怎么梳理接口入参
 * 做价格的计算 , 需要什么样的参数,才能计算价格
 */
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ChargingProcessParam {

    /**
     * 订单号
     */
    String orderNo;

    /**
     * 站id
     */
    Integer stationId;

    /**
     * 枪id
     */
    Integer gunId;

    /**
     * 用户信息 用户id
     */
    Integer userId;

    /**
     * 充电度数 充了多少度电
     */
    Float chargingCapacity;

}
